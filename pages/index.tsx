import React, { useState } from "react";
import Nav from "../components/Header/Nav";
import CartBar from "../components/Cart/CartBar";
import ImageSection from "../components/Image/ImageSection";
import DetailSection from "../components/Detail/DetailSection";
import CartSection from "../components/Cart/CartSection";
import product from "../components/Product";

/**
 * This is the home page!
 * @return {JSX.Element} : The JSX Code for home page.
 */

export type Ram = {
  id: number;
  size: string;
  price: number;
  stock: number;
};
export type Image = {
  name: string;
  path: string;
};
export type Variant = {
  id: number;
  color: string;
  ram: Ram[];
  images: Image[];
};

export type Product = {
  id: number;
  name: string;
  brand: string;
  battery: string;
  processor: string;
  storage: string;
  variant: Variant[];
};

export type CartItem = {
  id: any;
  name: string;
  brand: string;
  battery: string;
  processor: string;
  storage: string;
  variant: {
    id: number;
    color: string;
    ram: Ram;
    images: Image;
  };
};

const Home: React.FC = (): JSX.Element => {
  const [CartItems, setCartItems] = useState<CartItem[]>([]);
  const [variant, setVariant] = useState<Variant>(product.variant[0]);
  const [image, setImage] = useState<Image>(product.variant[0].images[0]);
  const [ram, setRam] = useState<Ram>(product.variant[0].ram[0]);
  const [show, setShow] = useState<boolean>(false);

  const handleClose = (): void => setShow(false);
  const handleShow = (): void => setShow(true);

  const onImageHover = (image: Image): void => {
    setImage(image);
  };

  const onSelectRam = (ram: Ram): void => {
    setRam(ram);
  };

  const onSelectVariant = (variant: Variant): void => {
    setVariant(variant);
    setImage(variant.images[0]);
  };

  const addToCart = (): void => {
    const CartItem: CartItem = {
      id: product.id,
      name: product.name,
      brand: product.brand,
      battery: product.battery,
      processor: product.processor,
      storage: product.storage,
      variant: {
        id: variant.id,
        color: variant.color,
        ram: ram,
        images: image,
      },
    };
    console.log(CartItem);
    setCartItems((prev) => {
      return [...prev, { ...CartItem }];
    });
  };

  return (
    <div>
      <Nav handleShow={handleShow} CartItems={CartItems} />
      <CartBar show={show} handleClose={handleClose} />
      <div style={{ margin: "10px", display: "flex" }}>
        <ImageSection
          images={variant.images}
          image={image}
          onImageHover={onImageHover}
        />
        <DetailSection
          ram={ram}
          product={product}
          variant={variant}
          onSelectVariant={onSelectVariant}
          onSelectRam={onSelectRam}
        />
        <CartSection variant={variant} addToCart={addToCart} />
      </div>
    </div>
  );
};

export default Home;
