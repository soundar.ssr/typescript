export default {
  id: 1,
  name: "Redmi 9A",
  brand: "Redmi",
  battery: "5000 mAh",
  processor: "2GHz Octa-core Helio G25 Processor",
  storage: "32GB",
  variant: [
    {
      id: 1,
      color: "Midnight Black",
      ram: [
        {
          id: 1,
          size: "2GB",
          price: 6799,
          stock: 5,
        },
        {
          id: 2,
          size: "3GB",
          price: 7799,
          stock: 10,
        },
      ],
      images: [
        {
          name: "main",
          path: "/images/black/main.jpg",
        },
        {
          name: "both",
          path: "/images/black/both.jpg",
        },

        {
          name: "back",
          path: "/images/black/back.jpg",
        },
        {
          name: "back_side",
          path: "/images/black/back_side.jpg",
        },
        {
          name: "side",
          path: "/images/black/side.jpg",
        },
        {
          name: "side_btn",
          path: "/images/black/side_btn.jpg",
        },
      ],
    },
    {
      id: 2,
      color: "Nature Green",
      ram: [
        {
          id: 1,
          size: "2GB",
          price: 6799,
          stock: 19,
        },
        {
          id: 2,
          size: "3GB",
          price: 7799,
          stock: 23,
        },
      ],
      images: [
        {
          name: "main",
          path: "/images/green/main.jpg",
        },
        {
          name: "both",
          path: "/images/green/both.jpg",
        },

        {
          name: "back",
          path: "/images/green/back.jpg",
        },
        {
          name: "bottom",
          path: "/images/green/bottom.jpg",
        },
        {
          name: "side",
          path: "/images/green/side.jpg",
        },
      ],
    },
    {
      id: 3,
      color: "Sea Blue",
      ram: [
        {
          id: 1,
          size: "2GB",
          price: 6799,
          stock: 23,
        },
        {
          id: 2,
          size: "3GB",
          price: 7799,
          stock: 90,
        },
        {
          id: 2,
          size: "5GB",
          price: 9799,
          stock: 98,
        },
      ],
      images: [
        {
          name: "main",
          path: "/images/blue/main.jpg",
        },
        {
          name: "both",
          path: "/images/blue/both.jpg",
        },
        {
          name: "front_side",
          path: "/images/blue/front_side.jpg",
        },
        {
          name: "back_side",
          path: "/images/blue/back_side.jpg",
        },
        {
          name: "bottom_side",
          path: "/images/blue/bottom_side.jpg",
        },
        {
          name: "side",
          path: "/images/blue/side.jpg",
        },
      ],
    },
  ],
};
