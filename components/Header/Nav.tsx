import { Navbar, Container } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { Cart3 as CartIcon } from "react-bootstrap-icons";
import { CartItem } from "../../pages/index";

/**
 * This is the home page!
 * @return {JSX.Element} : The JSX Code for Nav bar.
 */
interface Props {
  handleShow: () => void;
  CartItems: CartItem[];
}

const Homebar: React.FC<Props> = ({ handleShow, CartItems }): JSX.Element => {
  return (
    <>
      <Navbar
        style={{
          backgroundColor: "#f7941e !important",
          borderBottom: "1px solid rgba(173, 173, 173, 0.2)",
          minHeight: "10vh",
        }}
      >
        <Container>
          <Navbar.Brand href="#home">
            <div>
              <img
                alt=""
                src="/new-logo.png"
                height="40"
                className="d-inline-block align-top"
              />{" "}
            </div>
          </Navbar.Brand>
          <Navbar.Collapse
            className="justify-content-end"
            style={{
              cursor: "pointer",
            }}
            onClick={handleShow}
          >
            <CartIcon color="white" />
            <span
              className="badge badge-light"
              style={{
                marginTop: "-10px",
                borderRadius: "50%",
                cursor: "pointer",
                color: "black",
                background: "white",
              }}
            >
              {CartItems && CartItems.length}
            </span>
          </Navbar.Collapse>{" "}
        </Container>
      </Navbar>
    </>
  );
};

export default Homebar;
