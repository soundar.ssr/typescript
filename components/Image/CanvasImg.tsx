import React from "react";
import { Image } from "../../pages/index";
import styles from "../../styles/Home.module.css";

interface Props {
  image: Image;
}

const ProductImage: React.FC<Props> = ({ image }): JSX.Element => {
  return (
    <span>
      <img className={styles.canvas} src={image.path} />
    </span>
  );
};

export default ProductImage;
