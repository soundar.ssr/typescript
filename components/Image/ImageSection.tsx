/* eslint-disable react/prop-types */
import React from "react";
import ProductImage from "./ProductImg";
import CanvasImage from "./CanvasImg";
import { Image } from "../../pages/index";
import styles from "../../styles/Home.module.css";

interface Props {
  onImageHover: (arg0: Image) => void;
  image: Image;
  images: Image[];
}

const ImageSection: React.FC<Props> = ({
  onImageHover,
  images,
  image,
}): JSX.Element => {
  return (
    <div className={styles.imageSection}>
      <ProductImage onImageHover={onImageHover} images={images} />
      <CanvasImage image={image} />
    </div>
  );
};

export default ImageSection;
