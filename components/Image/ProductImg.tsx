import React from "react";
import { Image } from "../../pages/index";
import styles from "../../styles/Home.module.css";

interface Props {
  onImageHover: (arg0: Image) => void;
  images: Image[];
}

const ProductImage: React.FC<Props> = ({
  onImageHover,
  images,
}): JSX.Element => {
  return (
    <ul className={styles.small}>
      {images.map((img: Image) => {
        return (
          <li className={styles.listSm + " " + styles.selected} key={img.name}>
            <img
              className={styles.imgSm}
              onMouseOver={() => onImageHover(img)}
              src={img.path}
            />
          </li>
        );
      })}
    </ul>
  );
};

export default ProductImage;
