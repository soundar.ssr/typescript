import React from "react";
import { Variant } from "../../pages/index";
import styles from "../../styles/Home.module.css";

interface Props {
  variant: Variant;
  addToCart: () => void;
}

const ImageSection: React.FC<Props> = ({ variant, addToCart }): JSX.Element => {
  return (
    <div className={styles.cartSection}>
      <div className={styles.cart}>
        <button onClick={addToCart} className={styles.addBtn}>
          Add to Cart
        </button>
      </div>
    </div>
  );
};

export default ImageSection;
