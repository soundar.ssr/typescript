import React from "react";
import { Variant, Product, Ram } from "../../pages/index";
import styles from "../../styles/Home.module.css";

interface Props {
  variant: Variant;
  product: Product;
  ram: Ram;
  onSelectVariant: (arg0: Variant) => void;
  onSelectRam: (arg0: Ram) => void;
}

const DetailSection: React.FC<Props> = ({
  onSelectRam,
  onSelectVariant,
  variant,
  ram,
  product,
}): JSX.Element => {
  return (
    <div className={styles.detailSection}>
      <h3
        style={{ fontWeight: "bold" }}
      >{`${product.name} (${variant.color} ${ram.size} RAM ${product.storage} Storage) | 
       ${product.processor} Processor | ${product.battery}  Battery`}</h3>
      <h6>Brand : {product.brand}</h6>
      <h3 style={{ color: "#e77600" }}>Price : {ram.price}</h3>
      <h3 style={{ color: "green" }}>In Stock </h3>
      <h5>
        {" "}
        Sold By : <a>Poorvika Mobiles, Dindigul </a>
      </h5>
      <h5>
        {" "}
        <span style={{ color: "#82858a" }}>Color :</span> {variant.color}
      </h5>
      <div>
        {product.variant.map((variant) => {
          return (
            <span
              key={variant.id}
              style={{ padding: "5px" }}
              className={styles.listSm + " " + styles.selected}
            >
              <img
                className={styles.imgSm}
                onClick={() => onSelectVariant(variant)}
                src={variant.images[0].path}
              />
            </span>
          );
        })}
      </div>
      <h5>
        {" "}
        <span style={{ color: "#82858a" }}>Size Name :</span> {product.storage}
      </h5>
      <h5>
        {" "}
        <span style={{ color: "#82858a" }}>Style Name :</span> {ram.size}
      </h5>
      <div>
        {variant.ram.map((ra) => {
          return (
            <span
              style={{ padding: "5px" }}
              key={ra.id}
              className={styles.listSm + " " + styles.selected}
            >
              <button
                style={{ padding: "5px" }}
                onClick={() => onSelectRam(ra)}
              >
                {ra.size + " RAM"}{" "}
              </button>
            </span>
          );
        })}
      </div>

      <h5>
        {" "}
        Brand : <span style={{ color: "#82858a" }}>{product.brand}</span>{" "}
      </h5>
      <h5>
        {" "}
        Model : <span style={{ color: "#82858a" }}>{product.name}</span>{" "}
      </h5>
      <h5>
        {" "}
        Form Factor :{" "}
        <span style={{ color: "#82858a" }}>Touchscreen Phone</span>{" "}
      </h5>
      <h5>
        {" "}
        Memory Storage :{" "}
        <span style={{ color: "#82858a" }}>{product.storage}</span>{" "}
      </h5>
      <h5>
        {" "}
        OS : <span style={{ color: "#82858a" }}>Andriod</span>{" "}
      </h5>
    </div>
  );
};

export default DetailSection;
